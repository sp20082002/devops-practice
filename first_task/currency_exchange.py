import requests
import sys

def get_usd_to_rub_rate(API_LINK_CB_RF) -> float:
    response = requests.get(API_LINK_CB_RF)
    currency_exchange_rate = response.json().get('Valute').get('USD').get('Value')

    return currency_exchange_rate


def exchange_rub_to_usd(rub_amount: float, API_LINK_CB_RF: str) -> float:
    usd_rate = get_usd_to_rub_rate(API_LINK_CB_RF)
    return float(rub_amount)/usd_rate


def exchange_usd_to_rub(usd_amount: float, API_LINK_CB_RF: str) -> float:
    usd_rate = get_usd_to_rub_rate(API_LINK_CB_RF)
    return float(usd_amount)*usd_rate


if __name__ == '__main__':
    API_LINK_CB_RF = str(sys.argv[1])
    amount = int(sys.argv[2])
    currency_from = str(sys.argv[3])
    
    if currency_from == 'USD':
        print(exchange_usd_to_rub(usd_amount=amount, API_LINK_CB_RF=API_LINK_CB_RF))
    elif currency_from == 'RUB':
        print(exchange_rub_to_usd(rub_amount=amount, API_LINK_CB_RF=API_LINK_CB_RF))
    else: 
        raise 'No such currency my friend'

    
