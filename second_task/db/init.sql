CREATE TABLE students (
    id INT PRIMARY KEY,
    last_name VARCHAR(50) NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    birth_date DATE NOT NULL
);

/*Создаём студентов*/
INSERT INTO students VALUES (0, 'Markova', 'Karina', '10/10/2002');
INSERT INTO students VALUES (1, 'Netelev', 'Daniil', '10/10/2002');
INSERT INTO students VALUES (2, 'Poltavets', 'Stanislav', '08/20/2002');
INSERT INTO students VALUES (3, 'Popov', 'Andrew', '10/10/2002');
INSERT INTO students VALUES (4, 'Savin', 'Egor', '10/10/2002');
INSERT INTO students VALUES (5, 'Safronov', 'Alex', '12/29/2002');
INSERT INTO students VALUES (6, 'Serbin', 'Maks', '10/10/2002');
INSERT INTO students VALUES (7, 'Survillo', 'Oksana', '10/10/2002');
INSERT INTO students VALUES (8, 'Sisoeva', 'Anna', '10/10/2002');

CREATE TABLE record_books (
    id INT PRIMARY KEY,
    student_id INT NOT NULL,
    record_book_no VARCHAR(20) NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(id)
);

/*Создаём зачётки*/
INSERT INTO record_books VALUES (0, 0, 'Markova-05-20');
INSERT INTO record_books VALUES (1, 1, 'Netelev-05-20');
INSERT INTO record_books VALUES (2, 2, 'Poltavets-05-20');
INSERT INTO record_books VALUES (3, 3, 'Popov-05-20');
INSERT INTO record_books VALUES (4, 4, 'Savin-05-20');
INSERT INTO record_books VALUES (5, 5, 'Safronov-05-20');
INSERT INTO record_books VALUES (6, 6, 'Serbin-05-20');
INSERT INTO record_books VALUES (7, 7, 'Survillo-05-20');
INSERT INTO record_books VALUES (8, 8, 'Sisoeva-05-20');

CREATE TABLE subjects (
    id INT PRIMARY KEY,
    subject_name VARCHAR(100) NOT NULL
);

/*Создаём предметы */
INSERT INTO subjects VALUES (0, 'instrumental security analysis');
INSERT INTO subjects VALUES (1, 'big data');
INSERT INTO subjects VALUES (2, 'smart homes');
                             

CREATE TABLE exams (
    id INT PRIMARY KEY,
    record_book_id INT NOT NULL,
    subject_id INT NOT NULL,
    exam_date DATE NOT NULL,
    teacher_name VARCHAR(100) NOT NULL,
  	grade DECIMAL(4,2), -- добавляем поле для оценки
    FOREIGN KEY (record_book_id) REFERENCES record_books(id),
    FOREIGN KEY (subject_id) REFERENCES subjects(id)
);

/* Создаём экзамены */
INSERT INTO exams VALUES (0, 0, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 3.0);
INSERT INTO exams VALUES (1, 0, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 4.0);
INSERT INTO exams VALUES (2, 0, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 5.0);

INSERT INTO exams VALUES (3, 1, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 3.0);
INSERT INTO exams VALUES (4, 1, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 3.0);
INSERT INTO exams VALUES (5, 1, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 3.0);

INSERT INTO exams VALUES (6, 2, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 5.0);
INSERT INTO exams VALUES (7, 2, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 5.0);
INSERT INTO exams VALUES (8, 2, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 5.0);

INSERT INTO exams VALUES (9, 3, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 3.0);
INSERT INTO exams VALUES (10, 3, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 3.0);
INSERT INTO exams VALUES (11, 3, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 3.0);

INSERT INTO exams VALUES (12, 4, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 4.0);
INSERT INTO exams VALUES (13, 4, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 4.0);
INSERT INTO exams VALUES (14, 4, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 4.0);

INSERT INTO exams VALUES (15, 5, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 5.0);
INSERT INTO exams VALUES (16, 5, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 4.0);
INSERT INTO exams VALUES (17, 5, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 5.0);

INSERT INTO exams VALUES (18, 6, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 5.0);
INSERT INTO exams VALUES (19, 6, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 5.0);
INSERT INTO exams VALUES (20, 6, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 4.0);

INSERT INTO exams VALUES (21, 7, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 5.0);
INSERT INTO exams VALUES (22, 7, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 5.0);
INSERT INTO exams VALUES (23, 7, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 3.0);

INSERT INTO exams VALUES (24, 8, 0, '01/15/2023', 'Sadikov Ildar Venerovich', 5.0);
INSERT INTO exams VALUES (25, 8, 1, '01/20/2023', 'Lebedev Artyom Andreevich', 2.0);
INSERT INTO exams VALUES (26, 8, 2, '01/25/2023', 'Kotilevets Igor Denisovich', 5.0);

COMMIT;
