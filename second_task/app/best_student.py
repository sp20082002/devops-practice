import os
import sys
import psycopg2
from psycopg2 import sql
from tabulate import tabulate


def check_best_student(url: str, query: str):
    try:
        connection = psycopg2.connect(url)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()

        return result
    except Exception as e:
        print(f"Exception: {e}")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()


if __name__ == "__main__":
    #url = sys.argv[1]
    url = os.environ.get('DATABASE_URL')
    query = sql.SQL("""select 
            * 
          from (
            select
              e.record_book_id,
              rb.student_id,
              s.last_name,
              s.first_name,
              avg(e.grade) avg_grade
            FROM 
            	exams e
            	LEFT JOIN record_books rb ON e.record_book_id = rb.id
            	LEFT JOIN students s ON  rb.student_id = s.id
            GROUP BY
            	record_book_id,
          rb.student_id,
              s.last_name,
              s.first_name) as t1
          where avg_grade = (
            select
            max(avg_grade)
            from (
              select
              e.record_book_id,
              rb.student_id,
              s.last_name,
              s.first_name,
              avg(e.grade) avg_grade
              FROM 
              exams e
              LEFT JOIN record_books rb ON e.record_book_id = rb.id
              LEFT JOIN students s ON  rb.student_id = s.id
              GROUP BY
              record_book_id,
              rb.student_id,
              s.last_name,
              s.first_name
            ) as t)""")
    r = check_best_student(url=url, query=query)
    print(tabulate(r, headers=["record_book_id", "student_id",	"last_name",	"first_name",	"avg_grade"], tablefmt="pretty"))
